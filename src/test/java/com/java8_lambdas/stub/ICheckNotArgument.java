package com.java8_lambdas.stub;

public interface ICheckNotArgument {

	String execute();

	// void execute1(String msg);

}

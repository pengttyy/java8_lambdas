package com.java8_lambdas.stub;

public interface ICheckOneArgument {
	String execute(String name);
}

package com.java8_lambdas;

import static org.junit.Assert.*;

import java.util.function.BinaryOperator;
import java.util.function.Predicate;

import org.junit.Test;

import com.java8_lambdas.stub.ICheckNotArgument;
import com.java8_lambdas.stub.ICheckOneArgument;

public class LambdasTypeTest {

	/**
	 * 应用于只有一个方法的接口
	 */
	@Test
	public void testType_noArguments() {
		Runnable noArguments = () -> System.out.println("hello world!");
		noArguments.run();

		String value = "xxxx";
		ICheckNotArgument check = () -> value;// 返回值不需要return;
		assertEquals(value, check.execute());

		// 不能应用于类
		// CheckClass checkCls = () -> value;

		// 不能于抽象类
		// CheckAbstractClass checkACls = () -> value;

	}

	/**
	 * 应用于只有一个方法且方法只有一个参数的接口, 函数体只能有一个语句
	 * 
	 * @throws Exception
	 */
	@Test
	public void testOneArguments() throws Exception {
		ICheckOneArgument checkOneArg = name -> "hi," + name;
		String execute = checkOneArg.execute("pengttyy");

		assertEquals("hi,pengttyy", execute);
	}

	/**
	 * 函数体可以是多语句
	 * 
	 * @throws Exception
	 */
	@Test
	public void testMultiStatement() throws Exception {
		ICheckNotArgument check = () -> {
			String name = "pengttyy";
			int age = 10;
			return name + age;
		};

		assertEquals("pengttyy10", check.execute());
	}

	/**
	 * 多参数的lambda表达式
	 * 
	 * @throws Exception
	 */
	@Test
	public void testMultiArgument() throws Exception {
		/**
		 * x和y的类型是由编译器推断出来的
		 */
		BinaryOperator<Long> add = (x, y) -> x + y;
		long result = add.apply(10l, 20l);
		assertEquals(30l, result);
	}

	/**
	 * 引用值，而不是变量 lambda表达式中使用外部变量，这个变量必须是final的 即使没有用final 也必须是即成事实上的final
	 * 
	 * @throws Exception
	 */
	@Test
	public void testReferenceValue() throws Exception {
		int age = 10;
		// age = 22;
		// Local variable age defined in an enclosing scope must be final or
		// effectively final
		ICheckOneArgument checkOneArg = name -> "hi," + name + age;
		String execute = checkOneArg.execute("pengttyy");

		assertEquals("hi,pengttyy10", execute);

	}

	
}

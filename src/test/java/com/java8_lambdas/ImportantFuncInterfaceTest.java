package com.java8_lambdas;

import static org.junit.Assert.assertTrue;

import java.util.function.Predicate;
import java.util.function.Supplier;

import javax.swing.text.DateFormatter;

import org.junit.Test;

public class ImportantFuncInterfaceTest {

	@Test
	public void testPredicate() throws Exception {
		Predicate<String> isPengttyy = str -> str.equals("pengttyy");
		Predicate<String> isTtyy = str -> str.equals("ttyy");
		Predicate<String> or = isPengttyy.or(isTtyy);

		assertTrue(or.test("pengttyy"));
		assertTrue(or.test("ttyy"));

	}

	@Test
	public void testName() throws Exception {
		Supplier<DateFormatter> factory = () -> new DateFormatter();
		DateFormatter dateformatter = factory.get();
	}
}

package com.java8_lambdas.exercises.chapter3;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

public class StreamTest {

	@Test
	public void test1a() {
		Integer reduce = Stream.of(1, 2, 3, 4).reduce(0, (x, y) -> x + y);
		assertEquals(10, reduce.intValue());
	}

	@Test
	public void test1b() throws Exception {
		Stream<Actor> stream = Stream.of(new Actor("老王", "中国"), new Actor("小李",
				"日本"), new Actor("释迦哞尼", "印度"));
		List<String> list = stream.map(
				actor -> actor.getName() + "：" + actor.getNationality())
				.collect(Collectors.toList());
		assertTrue(list.contains("老王：中国"));
	}

	@Test
	public void test1c() throws Exception {
		List<Album> songSizeGt3 = initAlbums().stream()
				.filter(album -> album.getSongs().size() <= 3)
				.collect(Collectors.toList());
		assertEquals(2, songSizeGt3.size());
	}

	@Test
	public void test2() throws Exception {
		long count = initAlbums().stream()
				.flatMap(albums -> albums.getSongs().stream()).count();
		assertEquals(9, count);
	}

	/**
	 * 确定此函数有没有副作用
	 * 
	 * @throws Exception
	 */
	@Test
	public void test5() throws Exception {
		AtomicInteger count = new AtomicInteger(0);

		initAlbums().get(0).getSongs().forEach(song -> count.incrementAndGet());
		System.out.println(count.hashCode());
		System.out.println(count);
	}

	/**
	 * 统计字符串中的小写字母
	 * 
	 * @throws Exception
	 */
	@Test
	public void test6() throws Exception {
		String str = "abc42weAFD*&^";
		long countLowerCase = str.chars().filter(Character::isLowerCase)
				.count();
		assertEquals(5, countLowerCase);
	}

	/**
	 * 找出含有最多小写字母的字符串
	 * 
	 * @throws Exception
	 */
	@Test
	public void test7() throws Exception {
		Optional<String> maxLowerCaseChar = Stream.of("1a2b3c", "1a2b3c4d",
				"1a2b3c4d5e6f", "1a2b3c4d5e").max(
				Comparator.comparing(str -> str.chars()
						.filter(Character::isLowerCase).count()));
		assertEquals("1a2b3c4d5e6f", maxLowerCaseChar.get());
	}

	/**
	 * 用reduce和Lambda实现map操作
	 * 
	 * @throws Exception
	 */
	@Test
	public void test3101() throws Exception {

	}

	/**
	 * 用reduce和Lambda实现filter操作
	 * 
	 * @throws Exception
	 */
	@Test
	public void test3102() throws Exception {

	}

	private List<Album> initAlbums() {
		List<Album> albums = new ArrayList<Album>();
		albums.add(new Album("ab专辑", Arrays.asList("歌曲1", "歌曲2", "歌曲3", "歌曲4")));
		albums.add(new Album("ab专辑", Arrays.asList("歌曲1", "歌曲2", "歌曲3")));
		albums.add(new Album("ab专辑", Arrays.asList("歌曲1", "歌曲2")));
		return albums;
	}

}

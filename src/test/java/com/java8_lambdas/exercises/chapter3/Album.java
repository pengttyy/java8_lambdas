package com.java8_lambdas.exercises.chapter3;

import java.util.List;

public class Album {
	private String albumName;
	private List<String> songs;

	public Album(String albumName, List<String> songs) {
		super();
		this.albumName = albumName;
		this.songs = songs;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public List<String> getSongs() {
		return songs;
	}

	public void setSongs(List<String> songs) {
		this.songs = songs;
	}
}

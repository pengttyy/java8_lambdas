package com.java8_lambdas.exercises.chapter2;

import java.text.DateFormat;
import java.util.Date;
import java.util.function.Function;

import org.junit.Test;
import static org.junit.Assert.*;

public class LambdaExercisesTest {

	@Test
	public void test_1c() {
		Function<Long, Long> fun1 = x -> x + 1;
		// Function<Long, Long> fun2 = (x, y) -> x + 1;
		// Function<Long, Long> fun3 = x -> x == 1;
	}

	@Test
	public void test2b() throws Exception {
		ThreadLocal<DateFormat> local = ThreadLocal
				.withInitial(() -> DateFormat.getInstance());
		DateFormat patten = local.get();
		String msg = patten.format(new Date(0));

		assertEquals("70-1-1 上午8:00", msg);
	}
}

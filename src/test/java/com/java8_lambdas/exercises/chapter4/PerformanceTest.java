package com.java8_lambdas.exercises.chapter4;

import static org.junit.Assert.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

public class PerformanceTest {

	@Test
	public void testArtistNotBond() throws Exception {
		assertArtistNames("bob");
	}

	@Test
	public void testArtistBond() throws Exception {
		assertArtistNames("bob", "joh");
	}

	private void assertArtistNames(String... expected) {
		Performance performance = new PerformanceImp(new Artist(expected));
		Stream<String> artistNames = performance.getAllMusicians();
		List<String> names = artistNames.collect(Collectors.toList());
		assertArrayEquals(expected, names.toArray());
	}

}

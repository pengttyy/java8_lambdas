package com.java8_lambdas.exercises.chapter4;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Artist {
	private List<String> members;

	public Artist(String... member) {
		this.members = Arrays.asList(member);
	}

	public Stream<String> getMembers() {
		return members.stream();
	}

}

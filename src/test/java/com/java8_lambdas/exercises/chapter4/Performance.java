package com.java8_lambdas.exercises.chapter4;

import java.util.stream.Stream;

public interface Performance {
	String getName();

	Stream<Artist> getMusicians();

	Stream<String> getAllMusicians();
}

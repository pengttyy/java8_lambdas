package com.java8_lambdas.exercises.chapter4;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class PerformanceImp implements Performance {
	private List<Artist> artists;

	public PerformanceImp(Artist... artist) {
		this.artists = Arrays.asList(artist);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Stream<Artist> getMusicians() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Stream<String> getAllMusicians() {
		return this.artists.stream().flatMap(artist -> artist.getMembers());
	}
}
